/* 
 * File:   Homework 1
 * Author: Ahmad Yamin
 * Date: March 5, 2022
 */

//System Libraries
#include <iostream>
#include <iomanip>


using namespace std;

int main(int argc, char** argv) {
    cout << "Homework #1:" << endl << endl;
    
    //Problem #1:
    int x = 50;
    int y = 100;
    int Total = x + y;
    cout << "1) Given x = " << x << " and y = " << y;
    cout << ", the sum of these variables is " << Total << endl << endl;
    
    //Problem #2:
    double Purchase = 95.00;
    double SalesTax = Purchase * (0.06);
    double Total2 = SalesTax + Purchase;
    cout << "2) The total is $" << Total2 << " with $" << SalesTax;
    cout << " being the tax." << endl << endl;
    
    //Problem #3:
    double MealChrg = 88.67;
    double Tax = MealChrg * 0.0675;
    double Tip = (MealChrg + Tax) * 0.2;
    double Total3 = MealChrg + Tax + Tip;
    cout<<fixed<<setprecision(2)<<showpoint;
    cout << "3) The total for the meal is $" << MealChrg;
    cout << ", tax is going to be $" << Tax << ", the tip is $";
    cout << Tip << " and the total is going to be $" << Total3 << endl << endl;
    
    
    //Problem #4:
    int Gas = 15;
    int Range = 375;
    int MPG = Range/Gas;
    cout << "4) If a car has a gas tank capacity of " << Gas;
    cout << " and can travel " << Range << " on a full tank";
    cout << ", then it gets around " << MPG << " mpg." << endl;
    
    //Problem #5:
    cout << "5)\nName: Ahmad Yamin\nPhone:(951)999-2254\nMajor: Aerospace\n\n";
    
    //Problem #6:
    
    //1) Display 8
    int number_of_pods, peas_per_pod, total_peas;
    
    cout << "Press return after entering a number.\n";
    cout << "Enter the number of pods:\n";
    
    cin >> number_of_pods;
    
    cout << "Enter the number of peas in a pod:\n";
    cin >> peas_per_pod;
    total_peas = number_of_pods * peas_per_pod;
    cout << "If you have ";
    cout << number_of_pods;
    cout << " pea pods\n";
    cout << "and ";
    cout << peas_per_pod;
    cout << " peas in each pod, then\n";
    cout << "you have ";
    cout << total_peas;
    cout << " peas in all the pods.\n\n";
    
    //2) Added Hello and Goodbye
    //int number_of_pods, peas_per_pod, total_peas;
    
    cout << "Hello\n";
    cout << "Press return after entering a number.\n";
    cout << "Enter the number of pods:\n";
    
    cin >> number_of_pods;
    
    cout << "Enter the number of peas in a pod:\n";
    cin >> peas_per_pod;
    total_peas = number_of_pods * peas_per_pod;
    cout << "If you have ";
    cout << number_of_pods;
    cout << " pea pods\n";
    cout << "and ";
    cout << peas_per_pod;
    cout << " peas in each pod, then\n";
    cout << "you have ";
    cout << total_peas;
    cout << " peas in all the pods.\nGoodbye\n\n";
    
    //3)Changed * to /
    //int number_of_pods, peas_per_pod, total_peas;
    
    cout << "Hello\n";
    cout << "Press return after entering a number.\n";
    cout << "Enter the number of pods:\n";
    
    cin >> number_of_pods;
    
    cout << "Enter the number of peas in a pod:\n";
    cin >> peas_per_pod;
    total_peas = number_of_pods / peas_per_pod;
    cout << "If you have ";
    cout << number_of_pods;
    cout << " pea pods\n";
    cout << "and ";
    cout << peas_per_pod;
    cout << " peas in each pod, then\n";
    cout << "you have ";
    cout << total_peas;
    cout << " peas in all the pods.\nGoodbye\n\n";
    
    //4)Changed * to +
    //int number_of_pods, peas_per_pod, total_peas;
    
    cout << "Hello\n";
    cout << "Press return after entering a number.\n";
    cout << "Enter the number of pods:\n";
    
    cin >> number_of_pods;
    
    cout << "Enter the number of peas in a pod:\n";
    cin >> peas_per_pod;
    total_peas = number_of_pods + peas_per_pod;
    cout << "If you have ";
    cout << number_of_pods;
    cout << " pea pods\n";
    cout << "and ";
    cout << peas_per_pod;
    cout << " peas in each pod, then\n";
    cout << "you have ";
    cout << total_peas;
    cout << " peas in all the pods.\nGoodbye\n";
    
    return 0;
}

