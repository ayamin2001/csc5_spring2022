/* 
 * File:   Homework 2
 * Author: Ahmad Yamin
 * Date: March 11, 2022
 */

//System Libraries
#include <iostream>
#include <iomanip>


using namespace std;

int main(int argc, char** argv) {
    //Problem #1
        // A) An example of a syntax (compiler error) would be
        //    if a line of code didn't have ';' at the end of it.
    
        // B) An example of a logic error would be if instead
        //    of using '==', the programmer uses '=' and gets the wrong output.
    
    //Problem #2
        // A)   Output from the code snapshot:
        //      Hello my name is Calvin
        //      Hi there Calvin! My name is Bill
        //      Nice to Meet You Bill.
        //      I am wondering if you're available for what?
        //      What? Bye Calvin!
        //      Uh... sure, bye !
        //
        //
        //      CalvinCalvinCalvinCalvinCalvin
        //      BillBillBillBillBill

        // B)   9 Total Statements

        // C & D posted in canvas.

        // E)   Variables are useful in this example because instead
        //      of writing out the names every single time, we can just
        //      call the variable. It also allows us to make easier 
        //      changes if need be later on when creating the code.

    //Problem #3
        double name1q1,
               name1q2,
               name1q3,
               name1q4,
               name2q1,
               name2q2,
               name2q3,
               name2q4,
               name3q1,
               name3q2,
               name3q3,
               name3q4,
               q1avg,
               q2avg,
               q3avg,
               q4avg;

        
        cout << "Input the quiz scores for John, Mary, and Matthew.\n";
        cout <<"John's scores:" << endl;
        cin >> name1q1 >> name1q2 >> name1q3 >> name1q4;
        cout <<"Mary's scores:" << endl;
        cin >> name2q1 >> name2q2 >> name2q3 >> name2q4;
        cout <<"Matthew's scores:" << endl;
        cin >> name3q1 >> name3q2 >> name3q3 >> name3q4;
        
        q1avg = (name1q1 + name2q1 + name3q1)/3;
        q2avg = (name1q2 + name2q2 + name3q2)/3;
        q3avg = (name1q3 + name2q3 + name3q3)/3;
        q4avg = (name1q4 + name2q4 + name3q4)/3;
        
        cout << fixed << setprecision(0);
        cout << "\nName      Quiz 1     Quiz 2        Quiz 3      Quiz 4" << endl;
        cout << "----      ------     ------        ------      ------" << endl;
        cout << right << "John" << setw(12) << name1q1 << setw(11) << name1q2
                << setw(14) << name1q3 << setw(12) << name1q4 << endl;
        cout << right << "Mary" << setw(12) << name2q1 << setw(11) << name2q2
                << setw(14) << name2q3 << setw(12) << name2q4 << endl;
        cout << right << "Matthew" << setw(9) << name3q1 << setw(11) 
                << name3q2 << setw(14) << name3q3 << setw(12) 
                << name3q4 << endl << endl;
        cout << fixed << setprecision(2) << showpoint;
        cout << right << "Average" << setw(9) << q1avg << setw(11)
                << q2avg << setw(14) << q3avg << setw(12) << q4avg << endl;
        
    //Problem 4
        string name1,
               name2,
               food,
               adj,
               color,
               pet;
        int number;
        
        cout << "\nLet's play MadLibs. Enter the information asked for"
             << " to get started." << endl;
        
        cout << "Enter your name and another name:" << endl;
        cin >> name1 >> name2;
        cout << "Enter a food and color:" << endl;
        cin >> food >> color;
        cout << "Enter an animal and an adjective:" << endl;
        cin >> pet >> adj;
        cout << "Enter a number:" << endl;
        cin >> number;
        
        cout << "\nDear " << name2 << ",\n\nI am sorry that I am unable to " <<
                "turn in my homework at this time.\nFirst, I ate rotten " <<
                food << ", which made me turn " << color << 
                " and extremely ill. I came down with a fever of " << number <<
                ".\nNext, my " << adj << " pet " << pet << 
                " must have smelled the remains of the " << food <<
                "\non my homework because he ate it. I am currently " <<
                "rewriting my homework and hope you will accept it late.\n" <<
                "\nSincerely,\n" << name1;
                
    return 0;
}

